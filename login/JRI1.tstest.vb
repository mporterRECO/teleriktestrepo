Imports Telerik.TestStudio.Translators.Common
Imports Telerik.TestingFramework.Controls.TelerikUI.Blazor
Imports Telerik.TestingFramework.Controls.KendoUI.Angular
Imports Telerik.TestingFramework.Controls.KendoUI
Imports Telerik.WebAii.Controls.Html
Imports Telerik.WebAii.Controls.Xaml
Imports System
Imports System.Collections.Generic
Imports System.Text
Imports System.Linq

Imports ArtOfTest.Common.UnitTesting
Imports ArtOfTest.WebAii.Core
Imports ArtOfTest.WebAii.Controls.HtmlControls
Imports ArtOfTest.WebAii.Controls.HtmlControls.HtmlAsserts
Imports ArtOfTest.WebAii.Design
Imports ArtOfTest.WebAii.Design.Execution
Imports ArtOfTest.WebAii.ObjectModel
Imports ArtOfTest.WebAii.Silverlight
Imports ArtOfTest.WebAii.Silverlight.UI

Namespace recoMatrix1

    Public Class JustRemoveIt
        Inherits BaseWebAiiTest

#Region "[ Dynamic Pages Reference ]"

        Private _pages As Pages
        
        '''<summary>
        ''' Gets the Pages object that has references
        ''' to all the elements, frames or regions
        ''' in this project.
        '''</summary>
        Public ReadOnly Property Pages() As Pages
            Get
                If (_pages Is Nothing) Then
                    _pages = New Pages(Manager.Current)
                End If
                Return _pages
            End Get
        End Property
        
#End Region
        
        ' Add your test methods here...


        <CodedStep("Enter text 'zialio' in 'MTbNameText'")> _
        Public Sub JustRemoveIt_CodedStep3()
            'Enter text 'zialio' in 'MTbNameText'
            Actions.SetText(Pages.Matrix_Login.MTbNameText, "zialio")
            
        End Sub

        <CodedStep("New Coded Step")> _
        Public Sub JustRemoveIt_CodedStep()
            
        End Sub

        <CodedStep("Enter text 'In2knp2ts' in 'MTbPasswordPassword'")> _
        Public Sub JustRemoveIt_CodedStep1()
            'Enter text 'In2knp2ts' in 'MTbPasswordPassword'
            Actions.SetText(Pages.Matrix_Login.MTbPasswordPassword, "In2knp2ts")
            
        End Sub
    End Class
End Namespace